#!/bin/bash

  # 1. produce the object

  openCC -O3 -c wavelet2s.cpp \
         -o wavelet2s.o

  # 2. produce the static library

  ar rcs libwavelib2s.a wavelet2s.o
