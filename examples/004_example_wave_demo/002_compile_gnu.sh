#!/bin/bash

  # 1. compile

  g++   -O3                                     \
        -Wall                                   \
        -std=c++0x                              \
        driver_program.cpp                      \
        /opt/wavelib/gnu/lib/libwavelib2s.a \
        /opt/fftw/334/gnu_480/lib/libfftw3.a    \
        /opt/opencv/249/lib/*                   \
        -o x_gnu
