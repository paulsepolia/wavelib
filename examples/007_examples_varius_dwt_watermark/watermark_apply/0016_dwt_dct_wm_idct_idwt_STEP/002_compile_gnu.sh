#!/bin/bash

  # 1. compile

  g++   -O3                                          \
        -Wall                                        \
        -std=c++0x                                   \
        driver_program.cpp                           \
        /opt/wavelib/gnu/lib/libwavelib2s.a      \
	/opt/fftw/334/gnu/lib/libfftw3_omp.a     \
        /opt/fftw/334/gnu/lib/libfftw3.a         \
        /opt/fftw/334/gnu/lib/libfftw3_threads.a \
        /opt/opencv/249/lib/*                        \
        -lm                                          \
        -fopenmp                                     \
        -lpthread                                    \
        -o x_gnu
