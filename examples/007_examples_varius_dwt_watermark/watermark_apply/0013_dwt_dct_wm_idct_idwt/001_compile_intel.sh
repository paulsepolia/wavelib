#!/bin/bash

  # 1. compile

  icpc -O3                                   \
       -xHost                                \
       -Wall                                 \
       -std=c++11                            \
       -wd2012                               \
       driver_program.cpp                    \
       /opt/wavelib/intel/lib/libwavelib2s.a \
       /opt/fftw/334/intel/lib/libfftw3.a    \
       /opt/opencv/249/lib/*                 \
       -o x_intel
