
//=============================================================//
//  Description: 				               //
//						               //
//  1 --> import image                                         //
//  2 --> get the data image matrix                            //
//  3 --> apply the dwt to the image matrix                    //
//  4 --> get the dwt image data                               //
//  5 --> construct the dwt submatrices                        //
//  6 --> apply to some of them a dct                          //
//  7 --> modify some elements of the dct matrices (watermark) //
//  8 --> apply the inverse dct                                //
//  9 --> construct the dwt image data                         //
// 10 --> apply the inverse dwt                                //
// 11 --> construct the new watermarked image data             //
// 12 --> display the new watermarked image                    //
// 13 --> save the new watermarked image                       //
//                                                             //
//=============================================================//

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <iomanip>

#include "wavelet2s.h"
#include "cv.h"
#include "highgui.h"
#include "cxcore.h"

#include "functions.h"

using namespace cv;

using std::fixed;
using std::showpos;
using std::setprecision;
using std::showpoint;
using std::string;
using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
	//============//
	// input data //
	//============//

	// image file path

	const string imageDir = "../../data_files/";

	// image file name

	const string imageName = "lenaHD.bmp";

	// DWT type applied

	const string dwt_type = "db4";

	// refinement level for the above DWT type

        const int refinement_level = 3;

	// parameter to identify the number of types of the dwt submatrices

	const int TYPE_MAX = 100;

	//================================//
	// local variables and parameters //
	//================================//

	// image file path

	const string imageFile = imageDir + imageName;

	// container to hold the image data

	vector<vector<double>> image_data;

	// variables for image_data dimension

	int image_data_rows;
        int image_data_cols;

	// vector to hold the dimensions of the dwt submatrices

	vector<int> dwt_submat_dim;

	// vector to hold ALL the dwt data (dwt submatrices elements)

        vector<double> dwt_data;

        // it has size always size 1 and the value of that unique
        // element is the number of the different dwt submatrices types

        vector<double> dwt_flag;

	// total dwt data elements

	int dwt_data_elem_tot;

	// number of types of dwt submatrices

	int dwt_submat_types;

	// variables to hold the number of dwt submatrices of each type
	// note: for "db4" and "refinement_level" = 3
	// always I get back 3 (three) different submatrix types

	int dwt_mat_num_a = -1;
	int dwt_mat_num_b = -1;
	int dwt_mat_num_c = -1;

	// total elements of each different dwt type submatrix

	int tot_elem_a;
        int tot_elem_b;
        int tot_elem_c;

	// total elements of each matrix type

	int tot_elem_all_a;
        int tot_elem_all_b;
        int tot_elem_all_a_b;

	// rows and cols for the matrix types

	int rows_a;
	int rows_b;
	int rows_c;
	int cols_a;
	int cols_b;
	int cols_c;

	// index for matrix of type {a,b,c}

	int ima;
        int imb;
        int imc;

	// help variables

	int help_index;
	int sentinel;
	int offset;
	int index_tmp;

	//============================//
	// main execution starts here //
	//============================//

	// adjust the output format

	cout << fixed;
	cout << setprecision(16);
	cout << showpos;
	cout << showpoint;

	// import the image

	IplImage * img;

	cout << " -->  1 --> read image" << endl;

	img = cvLoadImage(imageFile.c_str());

	// test if the image has been read with success

	cout << " -->  2 --> test if image is read with success" << endl;

	if (!img)
	{
		cout << " --> can not read the image" << endl;
		cout << " --> try different image format" << endl;
		cout << " --> enter an interer to exit!" << endl;

		cin >> sentinel;
		exit(-1);
	}

	cout << " -->  3 --> image read with success" << endl;

	// get some properties of the image

	cout << " -->  4 --> get some properties of the image" << endl;

	int height;
	int width;

	height = img->height;
	width = img->width;

	// Mat class
	// get the data of the image
	// store the data of the image in imageData

	cout << " -->  6 --> store the image data in a matrix-like container" << endl;

        image_data_rows = height;
        image_data_cols = width;

	// Mat object

	Mat matimg(img);

	// resize image_data container to the dimension of the image

	cout << " -->  7 --> resize the image_data container" << endl;

	image_data.resize(image_data_rows);

	for(int i = 0; i != image_data_rows; ++i)
	{
		image_data[i].resize(image_data_cols);
	}

	// build image_data container

	cout << " -->  8 --> build the image_data container" << endl;

	for (int i = 0; i != image_data_rows; ++i)
	{
		for (int j = 0; j != image_data_cols; ++j)
		{
			unsigned char tmp;
			tmp = ((uchar*) matimg.data + i * matimg.step)[j * matimg.elemSize() + 1];
			image_data[i][j] = static_cast<double>(tmp);
		}
	}

	// apply the DWT to the image_data container

	cout << " -->  9 --> apply the 2D DWT transform to the image_data container" << endl;

	dwt_2d_sym(image_data,
		   refinement_level,
		   dwt_type,
                   dwt_data,
		   dwt_flag,
                   dwt_submat_dim);


	// get the number of dwt submatrices type --> method 1

	dwt_submat_types = dwt_submat_dim.size()/2;

	cout << " --> 10 --> # dwt submatrices type (method 1) = " 
	     << dwt_submat_types-1 << endl;

 	// get the number of dwt submatrices type --> method 2

        cout << " --> 11 --> # dwt submatrices type (method 2) = "
	     <<  static_cast<int>(dwt_flag[0]) << endl;

	// get the number of total dwt_data elements

	dwt_data_elem_tot = dwt_data.size();

	cout << " --> 12 --> total dwt data elements = " << dwt_data_elem_tot << endl;

	// set the number of rows and columns of each matrix type

	rows_a = dwt_submat_dim[0];
	cols_a = dwt_submat_dim[1];

	rows_b = dwt_submat_dim[2];
	cols_b = dwt_submat_dim[3];
	
	rows_c = dwt_submat_dim[4];
	cols_c = dwt_submat_dim[5];

	//===============================================//
	// find how many submatrices exist for each type //
	//===============================================//

        // rows*columns, total elements of matrices of type a

        tot_elem_a = rows_a * cols_a;

        // rows*columns, total elements of matrices of type b

        tot_elem_b = rows_b * cols_b;

        // rows*columns, total elements of matrices of type c

        tot_elem_c = rows_c * cols_c;

	// reset help_index 
	// (must be set to one - any other values indicates a mistake)

	cout << " --> 13 --> find how many submatrices exist for each type" << endl;

	help_index = 0;

	for(int ia = 0; ia != TYPE_MAX; ++ia)
	{
		for (int ib = 0; ib != TYPE_MAX; ++ib)
		{
			for (int ic = 0; ic != TYPE_MAX; ++ic)
			{
				if (ia * tot_elem_a +
				    ib * tot_elem_b +
				    ic * tot_elem_c ==
				    dwt_data_elem_tot)
				{
					++help_index;

					dwt_mat_num_a = ia;
					dwt_mat_num_b = ib;
					dwt_mat_num_c = ic;
				}
			}
		}
	}
	
	// test if the above algorithm works ok
	// help_index must be 1 (one)

	cout << " --> 14 --> test if the logic is okay" << endl;

	if (help_index != 1) 
	{
		cout << " --> Error in the algorithm" << endl;
		cout << " --> Enter an integer to exit" << endl;
		cin >> sentinel;
		exit(-1);
	}

	cout << " --> 15 --> the logic is okay" << endl;

	// total elements of matrices of type {a,b,c}

	tot_elem_all_a = dwt_mat_num_a * tot_elem_a;
	tot_elem_all_b = dwt_mat_num_b * tot_elem_b;
        tot_elem_all_a_b = tot_elem_all_a + tot_elem_all_b;

	//=======================================//
	// declare and build the dwt submatrices //
	//=======================================//

	//
	// --> matrix --> 1 --> 141 x 246
	//

	// declare --> mat1

	double ** mat1 = new double * [rows_a];

	for (int i = 0; i != rows_a; ++i)
	{
		mat1[i] = new double [cols_a];
	}

	// build --> mat1

	ima = 1;
	offset = (ima-1)*tot_elem_a;

	for (int i = 0; i != rows_a; ++i)
	{
		for (int j = 0; j != cols_a; ++j)
		{
		  	index_tmp = offset + i*cols_a + j;
                        mat1[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
		}
	}

	//
	// --> matrix --> 2 --> 141 x 246
	//

        // declare --> mat2

        double ** mat2 = new double * [rows_a];

        for (int i = 0; i != rows_a; ++i)
        {
                mat2[i] = new double [cols_a];
        }

        // build --> mat2

	ima = 2;
        offset = (ima-1)*tot_elem_a;

        for (int i = 0; i != rows_a; ++i)
        {
                for (int j = 0; j != cols_a; ++j)
                {
		   	index_tmp = offset + i*cols_a + j;
                        mat2[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
		}
        }

	//
	// --> matrix --> 3 --> 141 x 246
	//

        // declare --> mat3

        double ** mat3 = new double * [rows_a];

        for (int i = 0; i != rows_a; ++i)
        {
                mat3[i] = new double [cols_a];
        }

        // build --> mat3

	ima = 3;
        offset = (ima-1)*tot_elem_a;

        for (int i = 0; i != rows_a; ++i)
        {
                for (int j = 0; j != cols_a; ++j)
                {
			index_tmp = offset + i*cols_a + j;
                        mat3[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
                }
        }

	//	
	// --> matrix --> 4 --> 141 x 246
	//

        // declare --> mat4

        double ** mat4 = new double * [rows_a];

        for (int i = 0; i != rows_a; ++i)
        {
                mat4[i] = new double [cols_a];
        }

        // build --> mat4

	ima = 4;
        offset = (ima-1)*tot_elem_a;

        for (int i = 0; i != rows_a; ++i)
        {
                for (int j = 0; j != cols_a; ++j)
                {
			index_tmp = offset + i*cols_a + j;
                        mat4[i][j] = dwt_data[index_tmp];
			//cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
                }
        }

	//
	// --> matrix --> 5 --> 275 x 485
	//

        // declare --> mat5

        double ** mat5 = new double * [rows_b];

        for (int i = 0; i != rows_b; ++i)
        {
                mat5[i] = new double [cols_b];
        }

        // build --> mat5

	imb = 1;
        offset = tot_elem_all_a + (imb-1)*tot_elem_b;

        for (int i = 0; i != rows_b; ++i)
        {
                for (int j = 0; j != cols_b; ++j)
                {
			index_tmp = offset + i*cols_b + j;
                        mat5[i][j] = dwt_data[index_tmp];
			//cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
                }
        }

	//
	// --> matrix --> 6 --> 275 x 485
	//

        // declare --> mat6

        double ** mat6 = new double * [rows_b];

        for (int i = 0; i != rows_b; ++i)
        {
                mat6[i] = new double [cols_b];
        }

        // build --> mat6

	imb = 2;
	offset = tot_elem_all_a + (imb-1)*tot_elem_b;

        for (int i = 0; i != rows_b; ++i)
        {
                for (int j = 0; j != cols_b; ++j)
                {
			index_tmp = offset + i*cols_b + j;
                        mat6[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
                }
        }

	//
	// --> matrix --> 7 --> 275 x 485
	//

        // declare --> mat7

        double ** mat7 = new double * [rows_b];

        for (int i = 0; i != rows_b; ++i)
        {
                mat7[i] = new double [cols_b];
        }

        // build --> mat7

	imb = 3;
	offset = tot_elem_all_a + (imb-1)*tot_elem_b;

        for (int i = 0; i != rows_b; ++i)
        {
                for (int j = 0; j != cols_b; ++j)
                {
		  	index_tmp = offset + i*cols_b + j;
                        mat7[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
                }
        }

	//
	// --> matrix --> 8 --> 543 x 963
	//

        // declare --> mat8

        double ** mat8 = new double * [rows_c];

        for (int i = 0; i != rows_c; ++i)
        {
                mat8[i] = new double [cols_c];
        }

        // build --> mat8

	imc = 1;
        offset = tot_elem_all_a_b + (imc-1)*tot_elem_c;

        for (int i = 0; i != rows_c; ++i)
        {
                for (int j = 0; j != cols_c; ++j)
                {
			index_tmp = offset + i*cols_c + j;
                        mat8[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;

                }
        }

	//
	// --> matrix --> 9 --> 543 x 963
	//

        // declare --> mat9

        double ** mat9 = new double * [rows_c];

        for (int i = 0; i != rows_c; ++i)
        {
                mat9[i] = new double [cols_c];
        }

        // build --> mat9

	imc = 2;
        offset = tot_elem_all_a_b + (imc-1)*tot_elem_c;

        for (int i = 0; i != rows_c; ++i)
        {
                for (int j = 0; j != cols_c; ++j)
                {
			index_tmp = offset + i*cols_c + j;
                        mat9[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
                }
        }

	//
	// --> matrix --> 10 --> 543 x 963
	//

        // declare --> mat10

        double ** mat10 = new double * [rows_c];

        for (int i = 0; i != rows_c; ++i)
        {
                mat10[i] = new double [cols_c];
        }

        // build --> mat10

	imc = 3;
        offset = tot_elem_all_a_b + (imc-1) * tot_elem_c;

        for (int i = 0; i != rows_c; ++i)
        {
                for (int j = 0; j != cols_c; ++j)
                {
			index_tmp = offset + i*cols_c + j;
                        mat10[i][j] = dwt_data[index_tmp];
                        //cout << index_tmp+1 << " --> " << dwt_data[index_tmp] << endl;
                }
        }

	//=======================================//
	// modify some values in matrix --> mat5 //
	//=======================================//

	// put the watermark
	// in locations which belong to matrix --> mat5 --> 275 x 485

	const int iL = 100;
	const int iR = 105;
	const int jL = 200;
	const int jR = 205;
	const double wmVal = 100;

        imb = 1;
        offset = tot_elem_all_a + (imb-1)*tot_elem_b;

        for (int i = 0; i != rows_b; ++i)
        {
                for (int j = 0; j != cols_b; ++j)
                {
                        index_tmp = offset + i*cols_b + j;
			if ((iL <= i) && (i <= iR) && (jL <= j) && (j <= jR))
                        { 
				dwt_data[index_tmp] = dwt_data[index_tmp] + wmVal;
			}	
                }
        }

	//===============================//
	// inverse dwt and image display //
	//===============================//

	// finding IDWT

	cout << " --> 16 --> finding the inverse dwt" << endl;

	// inverse dwt container

	vector<vector<double>> idwt_data; //(image_data_rows, vector<double>(image_data_cols));

	// resize the inverse dwt container

	idwt_data.resize(image_data_rows);

        for(int i = 0; i != image_data_rows; ++i)
        {
                idwt_data[i].resize(image_data_cols);
        }

	// get the inverse dwt data

	idwt_2d_sym(dwt_data, dwt_flag, dwt_type, idwt_data, dwt_submat_dim);

	// reconstruct the image using the idwt_data

	cout << " --> 17 --> reconstruct the image" << endl;

	IplImage * dvImg;
	CvSize dvSize;

	dvSize.width = idwt_data[0].size();
	dvSize.height = idwt_data.size();

	dvImg = cvCreateImage(dvSize, 8, 1);

	for (int i = 0; i != dvSize.height; ++i)
	{
		for (int j = 0; j != dvSize.width; ++j)
		{
			((uchar*)(dvImg->imageData + dvImg->widthStep*i))[j] =
				static_cast<char>(idwt_data[i][j]);
		}
	}

	// save to the hard disk the reconstructed image

	cvSaveImage("reconstructed_image.bmp", dvImg);

	//============================//
        // delete the dwt submatrices //
        //============================//

	cout << " --> XX --> delete the dwt submatrices" << endl;

	// type a --> mat_1, mat_2, mat_3, mat_4

	for (int i = 0; i != rows_a; ++i)
        {
		delete [] mat1[i];
		delete [] mat2[i];
		delete [] mat3[i];
                delete [] mat4[i];
        }

	delete [] mat1;
	delete [] mat2;
	delete [] mat3;
	delete [] mat4;

	// type b --> mat_5, mat_6, mat_7

        for (int i = 0; i != rows_b; ++i)
        {
                delete [] mat5[i];
                delete [] mat6[i];
                delete [] mat7[i];
        }

	delete [] mat5;
	delete [] mat6;
	delete [] mat7;

	// type c --> mat_8, mat_9, mat_10

	for (int i = 0; i != rows_c; ++i)
        {
                delete [] mat8[i];
                delete [] mat9[i];
                delete [] mat10[i];
        }

        delete [] mat8;
        delete [] mat9;
        delete [] mat10;

	// sentineling

	cout << " --> end" << endl;
	cin >> sentinel;

	return 0;
}

//======//
// FINI //
//======//

