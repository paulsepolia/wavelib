
//=============================================================//
//  Description: 				               //
//						               //
//  1 --> import image                                         //
//  2 --> get the data image matrix                            //
//  3 --> apply the dwt to the image matrix                    //
//  4 --> get the dwt image data                               //
//  5 --> construct the dwt submatrices                        //
//  6 --> apply to some of them a dct                          //
//  7 --> modify some elements of the dct matrices (watermark) //
//  8 --> apply the inverse dct                                //
//  9 --> construct the dwt image data                         //
// 10 --> apply the inverse dwt                                //
// 11 --> construct the new watermarked image data             //
// 12 --> display the new watermarked image                    //
// 13 --> save the new watermarked image                       // 
//=============================================================//

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <iomanip>
#include <iterator>

#include "functions.h"

#include "wavelet2s.h"
#include "cv.h"
#include "highgui.h"
#include "cxcore.h"

using namespace cv;

using std::fixed;
using std::showpos;
using std::setprecision;
using std::showpoint;
using std::string;
using std::copy;
using std::ostream_iterator;
using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
	//
	// --> input data
	//

	// image file path

	const string imageDir = "../../data_files/";

	// image file name

	const string imageName = "lenaHD.bmp";

	// DWT type applied

	const string dwt_type = "db4";

	// refinement level for the above DWT type

        const int refinement_level = 3;

	// parameter to identify the number of types of the dwt submatrices  

	const int TYPE_MAX = 100;

	//
	// --> local variables and parameters
	//

	// image file path

	const string imageFile = imageDir + imageName;

	// output iterator --> version --> alpha

	const ostream_iterator<int> cout_a(cout, "\n");

	// container to hold the image data

	vector<vector<double>> image_data;

	// variables for image_data dimension

	int image_data_rows;
        int image_data_cols;

	// contains the dimensions of the dwt submatrices

	vector<int> dwt_submat_dim;

	// contains ALL the dwt data (dwt submatrices)

        vector<double> dwt_data;

        // it has size always size 1 and the value that unique
        // elemnt is the number of the different dwt submatrices 

        vector<double> dwt_flag;

	// total dwt data elements

	int dwt_data_elem_tot;

	// number of types of dwt submatrices 

	int dwt_submat_types;

	// variables to hold the number of dwt submatrices of each type
	// note: for "db4" and £refinement_level" = 3
	//       always I gat back 3 (three) different submatrices types

	int dwt_mat_num_a;
	int dwt_mat_num_b;
	int dwt_mat_num_c;

	// help variables
	// total elements for each different dwt submatrix dimension

	int tot_elem_a;
        int tot_elem_b;
        int tot_elem_c;

	// help variables

	int help_index;
	int sentinel;
	int offset;

	//
	// --> main execution starts here
	//

	// adjust the output format

	cout << fixed;
	cout << setprecision(16);
	cout << showpos;
	cout << showpoint;

	// import the image

	IplImage * img;

	cout << " -->  1 --> read image" << endl;

	img = cvLoadImage(imageFile.c_str());

	// test if the image has been read with success

	cout << " -->  2 --> test if image is read with success" << endl;

	if (!img)
	{
		cout << " --> can not read the image" << endl;
		cout << " --> try different image format" << endl;
		cout << " --> enter an interer to exit!" << endl;

		int sentinel;
		cin >> sentinel;
		exit(-1);
	}

	cout << " -->  3 --> image read with sucess" << endl;

	// get some properties of the image

	cout << " -->  4 --> get some properties of the image" << endl;

	int height;
	int width;
	int nc;
	int pix_depth;

	height = img->height;
	width = img->width;
	nc = img->nChannels;
	pix_depth = img->depth;

	//cout << " --> image height   = " << height << endl;
	//cout << " --> image width    = " << width << endl;
	//cout << " --> image depth    = " << pix_depth << endl;
	//cout << " --> image channels = " << nc << endl;

	// HERE

	//// display and save the image

	//cout << " -->  5 --> display and save the image" << endl;

	//cvNamedWindow("Original Image", CV_WINDOW_AUTOSIZE);
	//cvShowImage("Original Image", img);
	//cvWaitKey();
	//cvDestroyWindow("Original Image");
	//cvSaveImage("orig.bmp", img);

	// Mat class
	// get the data of the image
	// store the data of the image in imageData

	cout << " -->  6 --> store the image data in a matrix-like container" << endl;

        image_data_rows = height;
        image_data_cols = width;

	// Mat object

	Mat matimg(img);

	// resize image_data container to the dimension of the image

	image_data.resize(image_data_rows);

	for(int i = 0; i != image_data_rows; ++i)
	{
		image_data[i].resize(image_data_cols);
	}

	// build image_data container

	for (int i = 0; i < image_data_rows; i++) 
	{
		for (int j = 0; j < image_data_cols; j++)
		{
			unsigned char tmp;
			tmp = ((uchar*) matimg.data + i * matimg.step)[j * matimg.elemSize() + 1];
			image_data[i][j] = static_cast<double>(tmp);
		}
	}

	// apply a 2D DWT transform to the image_data contianer
	// using the symetric extension algorithm

	// apply the DWT to the image_data container

	cout << " -->  7 --> apply the 2D DWT transform to the image_data container" << endl;

	dwt_2d_sym(image_data,
		   refinement_level,
		   dwt_type,
                   dwt_data,
		   dwt_flag,
                   dwt_submat_dim);

	// print out the dimensions

	//cout << " -->  8 --> dimensions of the dwt submatrices" << endl;

	//copy(dwt_submat_dim.begin(), dwt_submat_dim.end(), cout_a);

	// get the number of dwt submatrices type

	dwt_submat_types = dwt_submat_dim.size()/2;

	cout << " -->  9 --> # dwt submatrices type = " << dwt_submat_types-1 << endl;

	// get the number of total dwt_data elements

	dwt_data_elem_tot = dwt_data.size();

	cout << " --> 10 --> total dwt data elements = " << dwt_data_elem_tot << endl;

	//
	// --> find how many submatrices exist for each type
	//

	// --> help step

        // rows*columns, total elements of matrices of type a

        tot_elem_a = dwt_submat_dim[0] * dwt_submat_dim[1];

        // rows*columns, total elements of matrices of type b

        tot_elem_b = dwt_submat_dim[2] * dwt_submat_dim[3];

        // rows*columns, total elements of matrices of type c

        tot_elem_c = dwt_submat_dim[4] * dwt_submat_dim[5];

	// --> main step

	// reset help_index
	// must be set to 1 (one)
	//  

	help_index = 0;

	for(int ia = 0; ia != TYPE_MAX; ++ia)
	{
		for (int ib = 0; ib != TYPE_MAX; ++ib)
		{
			for (int ic = 0; ic != TYPE_MAX; ++ic)
			{
				if (ia * tot_elem_a + 
				    ib * tot_elem_b +
				    ic * tot_elem_c ==
				    dwt_data_elem_tot)
				{
					++help_index;

					dwt_mat_num_a = ia;
					dwt_mat_num_b = ib;
					dwt_mat_num_c = ic;
				}
			}
		}
	}
	
	// test if the above algorithm works ok
	// help_index must be 1 (one)

	if (help_index != 1) 
	{
		cout << " --> Error in the algorithm" << endl;
		cout << " --> Enter an integer to exit" << endl;
		cin >> sentinel;
		exit(-1);
	}

	cout << " --> " << dwt_flag[0] << endl;

	//	

	//
	// --> build the dwt submatrices
	//

	int kk1;
	int kk2;
	int kk3;

	// matrix --> 1 --> 141 x 246

	cout << "new --> 1 --> 141 x 246" << endl;
	
	kk1 = 0;	

	for (int i = kk1 * tot_elem_a ; i != (kk1+1) * tot_elem_a; i++)
	{
		//cout << i+1 << " --> " << dwt_data[i] << endl;
	}

	// declare --> mat1

	double ** mat1 = new double * [dwt_submat_dim[0]];

	for (int i = 0; i != dwt_submat_dim[0]; i++)
	{
		mat1[i] = new double [dwt_submat_dim[1]];
	}

	// build --> mat1

	offset = 0;

	for (int i = 0; i != dwt_submat_dim[0]; i++)
	{
		for (int j = 0; j != dwt_submat_dim[1]; j++)
		{
			mat1[i][j] = dwt_data[offset + i*dwt_submat_dim[1]+j];
		}
	}

	// output to verify the order

	//cout << " mat1[0][0] = " << mat1[0][0] << endl;
	//cout << " mat1[0][1] = " << mat1[0][1] << endl;
	//cout << " mat1[0][2] = " << mat1[0][2] << endl;
	//cout << " mat1[1][0] = " << mat1[1][0] << endl;
	//cout << " mat1[1][1] = " << mat1[1][1] << endl;
	//cout << " mat1[1][2] = " << mat1[1][2] << endl;

	// matrix --> 2 --> 141 x 246

	cout << "new --> 2 --> 141 x 246" << endl;

	kk1 = 1;

    	for (int i = kk1 * tot_elem_a; i != (kk1+1) * tot_elem_a; i++)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat2

        double ** mat2 = new double * [dwt_submat_dim[0]];

        for (int i = 0; i != dwt_submat_dim[0]; i++)
        {
                mat2[i] = new double [dwt_submat_dim[1]];
        }

        // build --> mat2

        offset = 1 * dwt_submat_dim[0] * dwt_submat_dim[1];

        for (int i = 0; i != dwt_submat_dim[0]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[1]; j++)
                {
                        mat2[i][j] = dwt_data[offset + i*dwt_submat_dim[1]+j];
                }
        }

	// output to verify the order

        //cout << " mat2[0][0] = " << mat2[0][0] << endl;
        //cout << " mat2[0][1] = " << mat2[0][1] << endl;
        //cout << " mat2[0][2] = " << mat2[0][2] << endl;
        //cout << " mat2[1][0] = " << mat2[1][0] << endl;
        //cout << " mat2[1][1] = " << mat2[1][1] << endl;
        //cout << " mat2[1][2] = " << mat2[1][2] << endl;

	// matrix --> 3 --> 141 x 246

        cout << "new --> 3 --> 141 x 246" << endl;

	kk1 = 2;

        for (int i = kk1 * tot_elem_a; i != (kk1+1)*tot_elem_a; i++)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat3

        double ** mat3 = new double * [dwt_submat_dim[0]];

        for (int i = 0; i != dwt_submat_dim[0]; i++)
        {
                mat3[i] = new double [dwt_submat_dim[1]];
        }

        // build --> mat3

        offset = 2 * dwt_submat_dim[0] * dwt_submat_dim[1];

        for (int i = 0; i != dwt_submat_dim[0]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[1]; j++)
                {
                        mat3[i][j] = dwt_data[offset + i*dwt_submat_dim[1]+j];
                }
        }

        // output to verify the order

        //cout << " mat3[0][0] = " << mat3[0][0] << endl;
        //cout << " mat3[0][1] = " << mat3[0][1] << endl;
        //cout << " mat3[0][2] = " << mat3[0][2] << endl;
        //cout << " mat3[1][0] = " << mat3[1][0] << endl;
        //cout << " mat3[1][1] = " << mat3[1][1] << endl;
        //cout << " mat3[1][2] = " << mat3[1][2] << endl;
	
	// matrix --> 4 --> 141 x 246

        cout << "new --> 4 --> 141 x 246" << endl;

	kk1 = 3;

        for (int i = kk1 * tot_elem_a; i != (kk1+1) * tot_elem_a; i++)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat4

        double ** mat4 = new double * [dwt_submat_dim[0]];

        for (int i = 0; i != dwt_submat_dim[0]; i++)
        {
                mat4[i] = new double [dwt_submat_dim[1]];
        }

        // build --> mat4

        offset = 3 * dwt_submat_dim[0] * dwt_submat_dim[1];

        for (int i = 0; i != dwt_submat_dim[0]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[1]; j++)
                {
                        mat4[i][j] = dwt_data[offset + i*dwt_submat_dim[1]+j];
                }
        }

        // output to verify the order

        //cout << " mat4[0][0] = " << mat4[0][0] << endl;
        //cout << " mat4[0][1] = " << mat4[0][1] << endl;
        //cout << " mat4[0][2] = " << mat4[0][2] << endl;
        //cout << " mat4[1][0] = " << mat4[1][0] << endl;
        //cout << " mat4[1][1] = " << mat4[1][1] << endl;
        //cout << " mat4[1][2] = " << mat4[1][2] << endl;

	// matrix --> 5 --> 275 x 485

        cout << "new --> 5 --> 275 x 485" << endl;

        kk1 = 4;
	kk2 = 1;

        for (int i = kk1*tot_elem_a+(kk2-1)*tot_elem_b; i != kk1*tot_elem_a+kk2*tot_elem_b; i++)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat5

        double ** mat5 = new double * [dwt_submat_dim[2]];

        for (int i = 0; i != dwt_submat_dim[2]; i++)
        {
                mat5[i] = new double [dwt_submat_dim[3]];
        }

        // build --> mat5

        offset = 4 * dwt_submat_dim[0] * dwt_submat_dim[1];

        for (int i = 0; i != dwt_submat_dim[2]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[3]; j++)
                {
                        mat5[i][j] = dwt_data[offset + i*dwt_submat_dim[3]+j];
                }
        }

        // output to verify the order

        //cout << " mat5[0][0] = " << mat5[0][0] << endl;
        //cout << " mat5[0][1] = " << mat5[0][1] << endl;
        //cout << " mat5[0][2] = " << mat5[0][2] << endl;
        //cout << " mat5[1][0] = " << mat5[1][0] << endl;
        //cout << " mat5[1][1] = " << mat5[1][1] << endl;
        //cout << " mat5[1][2] = " << mat5[1][2] << endl;

	// matrix --> 6 --> 275 x 485

        cout << "new --> 6 --> 275 x 485" << endl;

        kk1 = 4;
        kk2 = 2;

        for (int i = kk1*tot_elem_a+(kk2-1)*tot_elem_b; i != kk1*tot_elem_a+kk2*tot_elem_b; i++)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat6

        double ** mat6 = new double * [dwt_submat_dim[2]];

        for (int i = 0; i != dwt_submat_dim[2]; i++)
        {
                mat6[i] = new double [dwt_submat_dim[3]];
        }

        // build --> mat6

        offset = 4 * dwt_submat_dim[0]*dwt_submat_dim[1] + 
		 1 * dwt_submat_dim[2]*dwt_submat_dim[3];

        for (int i = 0; i != dwt_submat_dim[2]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[3]; j++)
                {
                        mat6[i][j] = dwt_data[offset + i*dwt_submat_dim[3]+j];
                }
        }

        // output to verify the order

        //cout << " mat6[0][0] = " << mat6[0][0] << endl;
        //cout << " mat6[0][1] = " << mat6[0][1] << endl;
        //cout << " mat6[0][2] = " << mat6[0][2] << endl;
        //cout << " mat6[1][0] = " << mat6[1][0] << endl;
        //cout << " mat6[1][1] = " << mat6[1][1] << endl;
        //cout << " mat6[1][2] = " << mat6[1][2] << endl;

	// matrix --> 7 --> 275 x 485

        cout << "new --> 7 --> 275 x 485" << endl;

        kk1 = 4;
        kk2 = 3;
        
        for (int i = kk1*tot_elem_a+(kk2-1)*tot_elem_b; i != kk1*tot_elem_a+kk2*tot_elem_b; i++)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat7

        double ** mat7 = new double * [dwt_submat_dim[2]];

        for (int i = 0; i != dwt_submat_dim[2]; i++)
        {
                mat7[i] = new double [dwt_submat_dim[3]];
        }

        // build --> mat7

        offset = 4 * dwt_submat_dim[0]*dwt_submat_dim[1] + 
		 2 * dwt_submat_dim[2]*dwt_submat_dim[3];

        for (int i = 0; i != dwt_submat_dim[2]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[3]; j++)
                {
                        mat7[i][j] = dwt_data[offset + i*dwt_submat_dim[3]+j];
                }
        }

        // output to verify the order

        //cout << " mat7[0][0] = " << mat7[0][0] << endl;
        //cout << " mat7[0][1] = " << mat7[0][1] << endl;
        //cout << " mat7[0][2] = " << mat7[0][2] << endl;
        //cout << " mat7[1][0] = " << mat7[1][0] << endl;
        //cout << " mat7[1][1] = " << mat7[1][1] << endl;
        //cout << " mat7[1][2] = " << mat7[1][2] << endl;

	// matrix --> 8 --> 543 x 963

        cout << "new --> 8 --> 543 x 963" << endl;

        kk1 = 4;
        kk2 = 3;
	kk3 = 1;

        for (int i = kk1 * tot_elem_a + kk2 * tot_elem_b + (kk3-1) * tot_elem_c;
	     i != kk1 * tot_elem_a + kk2 * tot_elem_b + kk3 * tot_elem_c;
	     ++i)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat8

        double ** mat8 = new double * [dwt_submat_dim[4]];

        for (int i = 0; i != dwt_submat_dim[4]; i++)
        {
                mat8[i] = new double [dwt_submat_dim[5]];
        }

        // build --> mat8

        offset = 4 * dwt_submat_dim[0]*dwt_submat_dim[1] + 
		 3 * dwt_submat_dim[2]*dwt_submat_dim[3];

        for (int i = 0; i != dwt_submat_dim[4]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[5]; j++)
                {
                        mat8[i][j] = dwt_data[offset + i*dwt_submat_dim[5]+j];
                }
        }

        // output to verify the order

        //cout << " mat8[0][0] = " << mat8[0][0] << endl;
        //cout << " mat8[0][1] = " << mat8[0][1] << endl;
        //cout << " mat8[0][2] = " << mat8[0][2] << endl;
        //cout << " mat8[1][0] = " << mat8[1][0] << endl;
        //cout << " mat8[1][1] = " << mat8[1][1] << endl;
        //cout << " mat8[1][2] = " << mat8[1][2] << endl;

	// matrix --> 9 --> 543 x 963

        cout << "new --> 9 --> 543 x 963" << endl;

        kk1 = 4;
        kk2 = 3;
        kk3 = 2;

        for (int i = kk1 * tot_elem_a + kk2 * tot_elem_b + (kk3-1) * tot_elem_c;
	     i != kk1 * tot_elem_a + kk2 * tot_elem_b + kk3 * tot_elem_c;
	     ++i)
        {
		//cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat9

        double ** mat9 = new double * [dwt_submat_dim[4]];

        for (int i = 0; i != dwt_submat_dim[4]; i++)
        {
                mat9[i] = new double [dwt_submat_dim[5]];
        }

        // build --> mat9

        offset = 4 * dwt_submat_dim[0]*dwt_submat_dim[1] + 
	         3 * dwt_submat_dim[2]*dwt_submat_dim[3] + 
		 1 * dwt_submat_dim[4]*dwt_submat_dim[5];

        for (int i = 0; i != dwt_submat_dim[4]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[5]; j++)
                {
                        mat9[i][j] = dwt_data[offset + i*dwt_submat_dim[5]+j];
                }
        }

        // output to verify the order

        //cout << " mat9[0][0] = " << mat9[0][0] << endl;
        //cout << " mat9[0][1] = " << mat9[0][1] << endl;
        //cout << " mat9[0][2] = " << mat9[0][2] << endl;
        //cout << " mat9[1][0] = " << mat9[1][0] << endl;
        //cout << " mat9[1][1] = " << mat9[1][1] << endl;
        //cout << " mat9[1][2] = " << mat9[1][2] << endl;

	// matrix --> 10 --> 543 x 963

        cout << "new --> 10 --> 543 x 963" << endl;

        kk1 = 4;
        kk2 = 3;
        kk3 = 3;

        for (int i = kk1*tot_elem_a+kk2*tot_elem_b+(kk3-1)*tot_elem_c;
	     i != kk1*tot_elem_a+kk2*tot_elem_b+kk3*tot_elem_c;
	     ++i)
        {
                //cout << i+1 << " --> " << dwt_data[i] << endl;
        }

        // declare --> mat10

        double ** mat10 = new double * [dwt_submat_dim[4]];

        for (int i = 0; i != dwt_submat_dim[4]; i++)
        {
                mat10[i] = new double [dwt_submat_dim[5]];
        }

        // build --> mat10

        offset = 4 * dwt_submat_dim[0] * dwt_submat_dim[1] + 
		 3 * dwt_submat_dim[2] * dwt_submat_dim[3] + 
		 2 * dwt_submat_dim[4] * dwt_submat_dim[5];

        for (int i = 0; i != dwt_submat_dim[4]; i++)
        {
                for (int j = 0; j != dwt_submat_dim[5]; j++)
                {
                        mat10[i][j] = dwt_data[offset + i*dwt_submat_dim[5]+j];
                }
        }

        // output to verify the order

        //cout << " mat10[0][0] = " << mat10[0][0] << endl;
        //cout << " mat10[0][1] = " << mat10[0][1] << endl;
        //cout << " mat10[0][2] = " << mat10[0][2] << endl;
        //cout << " mat10[1][0] = " << mat10[1][0] << endl;
        //cout << " mat10[1][1] = " << mat10[1][1] << endl;
        //cout << " mat10[1][2] = " << mat10[1][2] << endl;

	// the size of "flag" vector is 1
	// and the content is flag[0] = 3
	// I do not know the meaning of it

	cout << " --> flag data" << endl;

	for (unsigned int i = 0; i != dwt_flag.size(); i++)
	{
		//cout << i << " --> " << dwt_flag[i] << endl;
	}

	// the dimensions for the submartices
	// they are the same as in Mathematica

	cout << " --> dwt_submat_dim data" << endl;

	for (unsigned int i = 0; i != dwt_submat_dim.size(); i++)
	{
		cout << i << " --> " << dwt_submat_dim[i] << endl;
	}

	// This algorithm computes DWT of image of any given size.
	// Together with convolution and subsampling operations
	// it is clear that subsampled images are of different dwt_submat_dim than
	// dyadic dwt_submat_dim images. In order to compute the "effective" size of DWT
	// we do additional calculations

	double max;
	vector<int> dwt_submat_dim2;

	dwt_output_dim_sym(dwt_submat_dim, dwt_submat_dim2, refinement_level);

	// here are the contents of the dwt_submat_dim2 vector

        cout << " --> dwt_submat_dim2 data" << endl;

        for (unsigned int i = 0; i != dwt_submat_dim2.size(); i++)
        {
                cout << i << " --> " << dwt_submat_dim2[i] << endl;
        }

	// dwt_submat_dim2 is gives the integer vector that contains
	// the size of subimages that will
	// combine to form the displayed output image.
	// The last two entries of dwt_submat_dim2 gives the
	// size of DWT (rows_n by cols_n)

	int siz = dwt_submat_dim2.size();
	int rows_n = dwt_submat_dim2[siz-2];
	int cols_n = dwt_submat_dim2[siz-1];

	vector<vector<double>> dwtdisp(rows_n, vector<double>(cols_n));

	dispDWT(dwt_data, dwtdisp, dwt_submat_dim, dwt_submat_dim2, refinement_level);

	// dispDWT returns the 2D object dwtdisp 
	// which will be displayed using OPENCV's image
	// handling functions

	vector<vector<double>> dwt_output = dwtdisp;

	maxval(dwt_output, max);

	// max value is needed to take care of overflow which happens because
	// of convolution operations performed on unsigned 8 bit images

	// displaying scaled image
	// creating image in OpenCV

	IplImage * cvImg; // image used for output
	CvSize imgSize; // size of output image

	imgSize.width = cols_n;
	imgSize.height = rows_n;

	cvImg = cvCreateImage(imgSize, 8, 1);

	// dwt_hold is created to hold the dwt output
	// as further operations need to be
	// carried out on dwt_output in order to display scaled images

	vector<vector<double>> dwt_hold(rows_n, vector<double>(cols_n));

	dwt_hold = dwt_output;

	// Setting coefficients of created image to the scaled DWT output values

	for (int i = 0; i < imgSize.height; i++)
	{
		for (int j = 0; j < imgSize.width; j++)
		{
			if (dwt_output[i][j] <= 0.0)
			{
				dwt_output[i][j] = 0.0;
			}

			if (i <= (dwt_submat_dim2[0]) && j <= (dwt_submat_dim2[1]))
			{
				((uchar*)(cvImg->imageData + cvImg->widthStep*i))[j] =
					static_cast<char>((dwt_output[i][j] / max) * 255.0);
			}
			else
			{
				((uchar*)(cvImg->imageData + cvImg->widthStep*i))[j] =
					static_cast<char>(dwt_output[i][j]);
			}
		}
	}

	// display cvImg

	//cvNamedWindow("DWT Image", 1); // creation of a visualisation window

	//cvShowImage("DWT Image", cvImg); // image visualisation

	//cvWaitKey();

	//cvDestroyWindow("DWT Image");

	//cvSaveImage("dwt.bmp",cvImg);

	// finding IDWT

	vector<vector<double>> idwt_output(image_data_rows, vector<double>(image_data_cols));

	idwt_2d_sym(dwt_data, dwt_flag, dwt_type, idwt_output, dwt_submat_dim);

	// displaying reconstructed image

	IplImage * dvImg;
	CvSize dvSize;

	dvSize.width = idwt_output[0].size();
	dvSize.height = idwt_output.size();

	cout << " --> 5" << endl;

	cout << idwt_output.size() << " , " << idwt_output[0].size() << endl;

	cout << " --> 6" << endl;

	dvImg = cvCreateImage(dvSize, 8, 1);

	for (int i = 0; i < dvSize.height; i++)
	{
		for (int j = 0; j < dvSize.width; j++)
		{
			((uchar*)(dvImg->imageData + dvImg->widthStep*i))[j] =
				static_cast<char>(idwt_output[i][j]);
		}
	}

	// display reconstructed image

	//cvNamedWindow("Reconstructed Image", 1); // creation of a visualisation window
	//cvShowImage("Reconstructed Image", dvImg); // image visualisation
	//cvWaitKey();
	//cvDestroyWindow("Reconstructed Image");
	//cvSaveImage("recon.bmp", dvImg);

	return 0;
}

//======//
// FINI //
//======//

