
//=============================================================//
//  Description: 				               //
//						               //
//  1 --> import image                                         //
//  2 --> get the data image matrix                            //
//  3 --> apply the dwt to the image matrix                    //
//  4 --> get the dwt image data                               //
//  5 --> construct the dwt submatrices                        //
//  6 --> apply to some of them a dct                          //
//  7 --> modify some elements of the dct matrices (watermark) //
//  8 --> apply the inverse dct                                //
//  9 --> construct the dwt image data                         //
// 10 --> apply the inverse dwt                                //
// 11 --> construct the new watermarked image data             //
// 12 --> display the new watermarked image                    //
// 13 --> save the new watermarked image                       //
//                                                             //
//=============================================================//

#include <iostream>
#include <vector>
#include <string>
#include <cmath>
#include <algorithm>
#include <iomanip>

#include "wavelet2s.h"
#include "cv.h"
#include "highgui.h"
#include "cxcore.h"

#include "functions.h"

using namespace cv;

using std::fixed;
using std::showpos;
using std::setprecision;
using std::showpoint;
using std::string;
using std::cout;
using std::cin;
using std::endl;

// the main function

int main()
{
	//============//
	// input data //
	//============//

	// image file path

	const string imageDir = "../../data_files/";

	// image file name

	const string imageName = "lenaHD.bmp";

	// DWT type applied

	const string dwt_type = "db4";

	// refinement level for the above DWT type

        const int refinement_level = 3;

	// parameter to identify the number of types of the dwt submatrices

	const int TYPE_MAX = 100;

	//================================//
	// local variables and parameters //
	//================================//

	// image file path

	const string imageFile = imageDir + imageName;

	// container to hold the image data

	vector<vector<double>> image_data;

	// variables for image_data dimension

	int image_data_rows;
        int image_data_cols;

	// vector to hold the dimensions of the dwt submatrices

	vector<int> dwt_submat_dim;

	// vector to hold ALL the dwt data (dwt submatrices elements)

        vector<double> dwt_data;

        // it has size always size 1 and the value of that unique
        // element is the number of the different dwt submatrices types

        vector<double> dwt_flag;

	// total dwt data elements

	int dwt_data_elem_tot;

	// number of types of dwt submatrices

	int dwt_submat_types;

	// variables to hold the number of dwt submatrices of each type
	// note: for "db4" and "refinement_level" = 3
	// always I get back 3 (three) different submatrix types

	int dwt_mat_num_a = -1;
	int dwt_mat_num_b = -1;
	int dwt_mat_num_c = -1;

	// total elements of each different dwt type submatrix

	int tot_elem_a;
        int tot_elem_b;
        int tot_elem_c;

	// total elements of each matrix type

	int tot_elem_all_a;
        int tot_elem_all_b;
        int tot_elem_all_c;

	// rows and cols for the matrix types

	int rows_a;
	int rows_b;
	int rows_c;
	int cols_a;
	int cols_b;
	int cols_c;

	// index for matrix of type {a,b,c}

	int ima;
        int imb;
        int imc;

	// help variables

	int help_index;
	int sentinel;
	int offset;
	int tmp_index;

	//============================//
	// main execution starts here //
	//============================//

	// adjust the output format

	cout << fixed;
	cout << setprecision(16);
	cout << showpos;
	cout << showpoint;

	// import the image

	IplImage * img;

	cout << " -->  1 --> read image" << endl;

	img = cvLoadImage(imageFile.c_str());

	// test if the image has been read with success

	cout << " -->  2 --> test if image is read with success" << endl;

	if (!img)
	{
		cout << " --> can not read the image" << endl;
		cout << " --> try different image format" << endl;
		cout << " --> enter an interer to exit!" << endl;

		cin >> sentinel;
		exit(-1);
	}

	cout << " -->  3 --> image read with success" << endl;

	// get some properties of the image

	cout << " -->  4 --> get some properties of the image" << endl;

	int height;
	int width;
	//int nc;
	//int pix_depth;

	height = img->height;
	width = img->width;
	//nc = img->nChannels;
	//pix_depth = img->depth;

	//cout << " --> image height   = " << height << endl;
	//cout << " --> image width    = " << width << endl;
	//cout << " --> image depth    = " << pix_depth << endl;
	//cout << " --> image channels = " << nc << endl;

	// HERE

	//// display and save the image

	//cout << " -->  5 --> display and save the image" << endl;

	//cvNamedWindow("Original Image", CV_WINDOW_AUTOSIZE);
	//cvShowImage("Original Image", img);
	//cvWaitKey();
	//cvDestroyWindow("Original Image");
	//cvSaveImage("orig.bmp", img);

	// Mat class
	// get the data of the image
	// store the data of the image in imageData

	cout << " -->  6 --> store the image data in a matrix-like container" << endl;

        image_data_rows = height;
        image_data_cols = width;

	// Mat object

	Mat matimg(img);

	// resize image_data container to the dimension of the image

	cout << " -->  7 --> resize the image_data container" << endl;

	image_data.resize(image_data_rows);

	for(int i = 0; i != image_data_rows; ++i)
	{
		image_data[i].resize(image_data_cols);
	}

	// build image_data container

	cout << " -->  8 --> build the image_data container" << endl;

	for (int i = 0; i != image_data_rows; ++i)
	{
		for (int j = 0; j != image_data_cols; ++j)
		{
			unsigned char tmp;
			tmp = ((uchar*) matimg.data + i * matimg.step)[j * matimg.elemSize() + 1];
			image_data[i][j] = static_cast<double>(tmp);
		}
	}

	// apply a 2D DWT transform to the image_data contianer
	// using the symetric extension algorithm

	// apply the DWT to the image_data container

	cout << " -->  9 --> apply the 2D DWT transform to the image_data container" << endl;

	dwt_2d_sym(image_data,
		   refinement_level,
		   dwt_type,
                   dwt_data,
		   dwt_flag,
                   dwt_submat_dim);


	// get the number of dwt submatrices type --> method 1

	dwt_submat_types = dwt_submat_dim.size()/2;

	cout << " --> 10 --> # dwt submatrices type (method 1) = " 
	     << dwt_submat_types-1 << endl;

 	// get the number of dwt submatrices type --> method 2

        cout << " --> 11 --> # dwt submatrices type (method 2) = "
	     <<  static_cast<int>(dwt_flag[0]) << endl;

	// get the number of total dwt_data elements

	dwt_data_elem_tot = dwt_data.size();

	cout << " --> 12 --> total dwt data elements = " << dwt_data_elem_tot << endl;

	// set the number of rows and columns of each matrix type

	rows_a = dwt_submat_dim[0];
	cols_a = dwt_submat_dim[1];

	rows_b = dwt_submat_dim[2];
	cols_b = dwt_submat_dim[3];
	
	rows_c = dwt_submat_dim[4];
	cols_c = dwt_submat_dim[5];

	//===============================================//
	// find how many submatrices exist for each type //
	//===============================================//

        // rows*columns, total elements of matrices of type a

        tot_elem_a = rows_a * cols_a;

        // rows*columns, total elements of matrices of type b

        tot_elem_b = rows_b * cols_b;

        // rows*columns, total elements of matrices of type c

        tot_elem_c = rows_c * cols_c;

	// --> main step

	// reset help_index 
	// (must be set to one - any other values indicates a mistake)

	cout << " --> 13 --> find how many submatrices exist for each type" << endl;

	help_index = 0;

	for(int ia = 0; ia != TYPE_MAX; ++ia)
	{
		for (int ib = 0; ib != TYPE_MAX; ++ib)
		{
			for (int ic = 0; ic != TYPE_MAX; ++ic)
			{
				if (ia * tot_elem_a +
				    ib * tot_elem_b +
				    ic * tot_elem_c ==
				    dwt_data_elem_tot)
				{
					++help_index;

					dwt_mat_num_a = ia;
					dwt_mat_num_b = ib;
					dwt_mat_num_c = ic;
				}
			}
		}
	}
	
	// test if the above algorithm works ok
	// help_index must be 1 (one)

	cout << " --> 14 --> test if the logic is okay" << endl;

	if (help_index != 1) 
	{
		cout << " --> Error in the algorithm" << endl;
		cout << " --> Enter an integer to exit" << endl;
		cin >> sentinel;
		exit(-1);
	}

	cout << " --> 15 --> the logic is okay" << endl;

	// total elements of matrices of type {a,b,c}

	tot_elem_all_a = dwt_mat_num_a * tot_elem_a;
	tot_elem_all_b = dwt_mat_num_b * tot_elem_b;
        tot_elem_all_c = dwt_mat_num_c * tot_elem_c;

	//===========================//
	// build the dwt submatrices //
	//===========================//

	//
	// --> matrix --> 1 --> 141 x 246
	//

	// declare --> mat1

	double ** mat1 = new double * [rows_a];

	for (int i = 0; i != rows_a; ++i)
	{
		mat1[i] = new double [cols_a];
	}

	// build --> mat1

	ima = 1;
	offset = (ima-1)*tot_elem_a;

	for (int i = 0; i != rows_a; ++i)
	{
		for (int j = 0; j != cols_a; ++j)
		{
		  	tmp_index = offset + i*cols_a + j;
                        mat1[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
		}
	}

	//
	// --> matrix --> 2 --> 141 x 246
	//

        // declare --> mat2

        double ** mat2 = new double * [rows_a];

        for (int i = 0; i != rows_a; ++i)
        {
                mat2[i] = new double [cols_a];
        }

        // build --> mat2

	ima = 2;
        offset = (ima-1)*tot_elem_a;

        for (int i = 0; i != rows_a; ++i)
        {
                for (int j = 0; j != cols_a; ++j)
                {
		   	tmp_index = offset + i*cols_a + j;
                        mat2[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
		}
        }

	//
	// --> matrix --> 3 --> 141 x 246
	//

        // declare --> mat3

        double ** mat3 = new double * [rows_a];

        for (int i = 0; i != rows_a; ++i)
        {
                mat3[i] = new double [cols_a];
        }

        // build --> mat3

	ima = 3;
        offset = (ima-1)*tot_elem_a;

        for (int i = 0; i != rows_a; ++i)
        {
                for (int j = 0; j != cols_a; ++j)
                {
			tmp_index = offset + i*cols_a + j;
                        mat3[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
                }
        }

	//	
	// --> matrix --> 4 --> 141 x 246
	//

        // declare --> mat4

        double ** mat4 = new double * [rows_a];

        for (int i = 0; i != rows_a; ++i)
        {
                mat4[i] = new double [cols_a];
        }

        // build --> mat4

	ima = 4;
        offset = (ima-1)*tot_elem_a;

        for (int i = 0; i != rows_a; ++i)
        {
                for (int j = 0; j != cols_a; ++j)
                {
			tmp_index = offset + i*cols_a + j;
                        mat4[i][j] = dwt_data[tmp_index];
			cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
                }
        }

	//
	// --> matrix --> 5 --> 275 x 485
	//

        // declare --> mat5

        double ** mat5 = new double * [rows_b];

        for (int i = 0; i != rows_b; ++i)
        {
                mat5[i] = new double [cols_b];
        }

        // build --> mat5

	imb = 1;
        offset = tot_elem_all_a + (imb-1)*tot_elem_b;

        for (int i = 0; i != rows_b; ++i)
        {
                for (int j = 0; j != cols_b; ++j)
                {
			tmp_index = offset + i*cols_b + j;
                        mat5[i][j] = dwt_data[tmp_index];
			cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
                }
        }

	//
	// --> matrix --> 6 --> 275 x 485
	//

        // declare --> mat6

        double ** mat6 = new double * [rows_b];

        for (int i = 0; i != rows_b; ++i)
        {
                mat6[i] = new double [cols_b];
        }

        // build --> mat6

	imb = 2;
	offset = tot_elem_all_a + (imb-1)*tot_elem_b;

        for (int i = 0; i != rows_b; ++i)
        {
                for (int j = 0; j != cols_b; ++j)
                {
			tmp_index = offset + i*cols_b + j;
                        mat6[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
                }
        }

	//
	// --> matrix --> 7 --> 275 x 485
	//

        // declare --> mat7

        double ** mat7 = new double * [rows_b];

        for (int i = 0; i != rows_b; ++i)
        {
                mat7[i] = new double [cols_b];
        }

        // build --> mat7

	imb = 3;
	offset = tot_elem_all_a + (imb-1)*tot_elem_b;

        for (int i = 0; i != rows_b; ++i)
        {
                for (int j = 0; j != cols_b; ++j)
                {
		  	tmp_index = offset + i*cols_b + j;
                        mat7[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
                }
        }

	//
	// --> matrix --> 8 --> 543 x 963
	//

        // declare --> mat8

        double ** mat8 = new double * [rows_c];

        for (int i = 0; i != rows_c; ++i)
        {
                mat8[i] = new double [cols_c];
        }

        // build --> mat8

	imc = 1;
        offset = tot_elem_all_a + tot_elem_all_b + (imc-1)*tot_elem_c;

        for (int i = 0; i != rows_c; ++i)
        {
                for (int j = 0; j != cols_c; ++j)
                {
			tmp_index = offset + i*cols_c + j;
                        mat8[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;

                }
        }

	//
	// --> matrix --> 9 --> 543 x 963
	//

        // declare --> mat9

        double ** mat9 = new double * [rows_c];

        for (int i = 0; i != rows_c; ++i)
        {
                mat9[i] = new double [cols_c];
        }

        // build --> mat9

	imc = 2;
        offset = tot_elem_all_a + tot_elem_all_b + (imc-1)*tot_elem_c;

        for (int i = 0; i != rows_c; ++i)
        {
                for (int j = 0; j != cols_c; ++j)
                {
			tmp_index = offset + i*cols_c + j;
                        mat9[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
                }
        }

	//
	// --> matrix --> 10 --> 543 x 963
	//

        // declare --> mat10

        double ** mat10 = new double * [rows_c];

        for (int i = 0; i != rows_c; ++i)
        {
                mat10[i] = new double [cols_c];
        }

        // build --> mat10

	imc = 3;
        offset = tot_elem_all_a + tot_elem_all_b + (imc-1) * tot_elem_c;

        for (int i = 0; i != rows_c; ++i)
        {
                for (int j = 0; j != cols_c; ++j)
                {
			tmp_index = offset + i*cols_c + j;
                        mat10[i][j] = dwt_data[tmp_index];
                        cout << tmp_index+1 << " --> " << dwt_data[tmp_index] << endl;
                }
        }

	// REST MATERIAL

/*

	// This algorithm computes DWT of image of any given size.
	// Together with convolution and subsampling operations
	// it is clear that subsampled images are of different dwt_submat_dim than
	// dyadic dwt_submat_dim images. In order to compute the "effective" size of DWT
	// we do additional calculations

	double max;
	vector<int> dwt_submat_dim2;

	dwt_output_dim_sym(dwt_submat_dim, dwt_submat_dim2, refinement_level);

	// here are the contents of the dwt_submat_dim2 vector

        cout << " --> dwt_submat_dim2 data" << endl;

        for (unsigned int i = 0; i != dwt_submat_dim2.size(); i++)
        {
                cout << i << " --> " << dwt_submat_dim2[i] << endl;
        }

	// dwt_submat_dim2 is gives the integer vector that contains
	// the size of subimages that will
	// combine to form the displayed output image.
	// The last two entries of dwt_submat_dim2 gives the
	// size of DWT (rows_n by cols_n)

	int siz = dwt_submat_dim2.size();
	int rows_n = dwt_submat_dim2[siz-2];
	int cols_n = dwt_submat_dim2[siz-1];

	vector<vector<double>> dwtdisp(rows_n, vector<double>(cols_n));

	dispDWT(dwt_data, dwtdisp, dwt_submat_dim, dwt_submat_dim2, refinement_level);

	// dispDWT returns the 2D object dwtdisp 
	// which will be displayed using OPENCV's image
	// handling functions

	vector<vector<double>> dwt_output = dwtdisp;

	maxval(dwt_output, max);

	// max value is needed to take care of overflow which happens because
	// of convolution operations performed on unsigned 8 bit images

	// displaying scaled image
	// creating image in OpenCV

	IplImage * cvImg; // image used for output
	CvSize imgSize; // size of output image

	imgSize.width = cols_n;
	imgSize.height = rows_n;

	cvImg = cvCreateImage(imgSize, 8, 1);

	// dwt_hold is created to hold the dwt output
	// as further operations need to be
	// carried out on dwt_output in order to display scaled images

	vector<vector<double>> dwt_hold(rows_n, vector<double>(cols_n));

	dwt_hold = dwt_output;

	// Setting coefficients of created image to the scaled DWT output values

	for (int i = 0; i < imgSize.height; i++)
	{
		for (int j = 0; j < imgSize.width; j++)
		{
			if (dwt_output[i][j] <= 0.0)
			{
				dwt_output[i][j] = 0.0;
			}

			if (i <= (dwt_submat_dim2[0]) && j <= (dwt_submat_dim2[1]))
			{
				((uchar*)(cvImg->imageData + cvImg->widthStep*i))[j] =
					static_cast<char>((dwt_output[i][j] / max) * 255.0);
			}
			else
			{
				((uchar*)(cvImg->imageData + cvImg->widthStep*i))[j] =
					static_cast<char>(dwt_output[i][j]);
			}
		}
	}

	// display cvImg

	//cvNamedWindow("DWT Image", 1); // creation of a visualisation window

	//cvShowImage("DWT Image", cvImg); // image visualisation

	//cvWaitKey();

	//cvDestroyWindow("DWT Image");

	//cvSaveImage("dwt.bmp",cvImg);

	// finding IDWT

	vector<vector<double>> idwt_output(image_data_rows, vector<double>(image_data_cols));

	idwt_2d_sym(dwt_data, dwt_flag, dwt_type, idwt_output, dwt_submat_dim);

	// displaying reconstructed image

	IplImage * dvImg;
	CvSize dvSize;

	dvSize.width = idwt_output[0].size();
	dvSize.height = idwt_output.size();

	cout << " --> 5" << endl;

	cout << idwt_output.size() << " , " << idwt_output[0].size() << endl;

	cout << " --> 6" << endl;

	dvImg = cvCreateImage(dvSize, 8, 1);

	for (int i = 0; i < dvSize.height; i++)
	{
		for (int j = 0; j < dvSize.width; j++)
		{
			((uchar*)(dvImg->imageData + dvImg->widthStep*i))[j] =
				static_cast<char>(idwt_output[i][j]);
		}
	}

	// display reconstructed image

	//cvNamedWindow("Reconstructed Image", 1); // creation of a visualisation window
	//cvShowImage("Reconstructed Image", dvImg); // image visualisation
	//cvWaitKey();
	//cvDestroyWindow("Reconstructed Image");
	//cvSaveImage("recon.bmp", dvImg);
 // rest material
*/

	return 0;
}

//======//
// FINI //
//======//

