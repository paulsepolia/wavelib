#!/bin/bash

  # compile fortran object

  gfortran  -c         \
            -O3        \
            -std=f2008 \
            -Wall      \
            quick_sort_iter_2ar.f90

  # compile c++ object

  g++  -c         \
       -O3        \
       -std=c++0x \
       driver_program.cpp

  # link c++ and fortran objects

  g++  -O3                                          \
       -Wall                                        \
       -std=c++0x                                   \
       quick_sort_iter_2ar.o                        \
       driver_program.o                             \
       /opt/wavelib/gnu/lib/libwavelib2s.a      \
       /opt/fftw/334/gnu/lib/libfftw3_omp.a     \
       /opt/fftw/334/gnu/lib/libfftw3.a         \
       /opt/fftw/334/gnu/lib/libfftw3_threads.a \
       /opt/opencv/249/lib/*                        \
       -lm                                          \
       -fopenmp                                     \
       -lpthread                                    \
       -L/usr/lib/ -lgfortran                       \
       -o x_gnu

  # clean

  rm *.o

