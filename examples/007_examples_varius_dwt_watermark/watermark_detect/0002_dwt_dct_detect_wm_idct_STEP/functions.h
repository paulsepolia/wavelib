
//=======================//
//  Description: 	 //
//  functions definition //
//=======================//

#include <vector>

using std::vector;

// function --> the C interface to quick-sort fortran function

extern "C" void quick_sort_iter_2ar_(double *,  double *, const long &);

// function --> maxval

void * maxval(vector<vector<double>> & arr, double & max)
{
	max = 0;

	for (unsigned int i = 0; i < arr.size(); i++)
	{
		for (unsigned int j = 0; j < arr[0].size(); j++)
		{
			if (max <= arr[i][j])
			{
				max = arr[i][j];
			}
		}
	}

	return 0;
}

// function --> maxval1

void * maxval1(vector<double> & arr, double & max)
{
	max = 0;

	for (unsigned int i = 0; i < arr.size(); i++)
	{
		if (max <= arr[i])
		{
			max = arr[i];
		}
	}

	return 0;
}

//======//
// FINI //
//======//

