#!/bin/bash

  # compile fortran object

  ifort  -c           \
         -O3          \
         -warn all    \
         -heap-arrays \
         quick_sort_iter_2ar.f90

  # compile c++ object

  icpc  -c         \
        -O3        \
        -std=c++11 \
        driver_program.cpp


  # link fortran and c++ object

  icpc -O3                                    \
       -xHost                                 \
       -Wall                                  \
       -std=c++11                             \
       -wd2012                                \
       quick_sort_iter_2ar.o                  \
       driver_program.o                       \
       /opt/wavelib/intel/lib/libwavelib2s.a  \
       /opt/fftw/334/intel/lib/libfftw3.a     \
       /opt/fftw/334/intel/lib/libfftw3_omp.a \
       /opt/opencv/249/lib/*                  \
       -lm                                    \
       -openmp                                \
       -lpthread                              \
       -lifcore                               \
       -o x_intel

  # clean

  rm *.o
  rm *.mod
  rm quick_sort_iter_2ar__genmod.f90

