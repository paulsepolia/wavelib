
//===================================================================//
// Description: 						     //
// DWT of arbitrary size image using symmetric or periodic extension //
//===================================================================//

#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <complex>
#include <cmath>
#include <algorithm>
#include <iomanip>

#include "wavelet2s.h"
#include "cv.h"
#include "highgui.h"
#include "cxcore.h"


using namespace std;
using namespace cv;

using std::fixed;
using std::showpos;
using std::setprecision;
using std::showpoint;

// function --> maxval

void * maxval(vector<vector<double>> & arr, double & max)
{
	max = 0;

	for (unsigned int i = 0; i < arr.size(); i++)
	{
		for (unsigned int j = 0; j < arr[0].size(); j++)
		{
			if (max <= arr[i][j])
			{
				max = arr[i][j];
			}
		}
	}

	return 0;
}

// function --> maxval1

void * maxval1(vector<double> & arr, double & max)
{
	max = 0;

	for (unsigned int i = 0; i < arr.size(); i++)
	{
		if (max <= arr[i])
		{
			max = arr[i];
		}
	}

	return 0;
}

// the main function

int main() 
{
	// adjust the output format

	cout << fixed;
	cout << setprecision(16);
	cout << showpos;
	cout << showpoint;

	// import the image
	
	IplImage * img = cvLoadImage("lenaHD.bmp");

	// test if the image has been read with success
	
	if (!img)
	{
		
		cout << " --> can not read the image" << endl;
		cout << " --> try differnet image format" << endl;
		cout << " --> enter an interer to exit!" << endl;

		int sentinel;
		cin >> sentinel;
		exit(-1);
	}

	// get the dimensions of the image

	int height;
	int width;

	height = img->height;
	width = img->width;
	
	cout << " --> 1" << endl;

	cout << " --> image height = " << height << endl;
	cout << " --> image width = " << width << endl;

	// get the nChannels and the depth

	int nc = img->nChannels;
	int pix_depth = img->depth;

	cout << " --> 2" << endl;

	cout << " --> image depth = " << pix_depth << endl;
	cout << " --> image channels = " << nc << endl;

	cout << " --> 3" << endl;

	cvNamedWindow("Original Image", CV_WINDOW_AUTOSIZE);

	cout << " --> 4" << endl;

	cvShowImage("Original Image", img);

	cvWaitKey();

	cvDestroyWindow("Original Image");

	cvSaveImage("orig.bmp", img);

	// get the rows and the cols of the image

	int rows = static_cast<int>(height);
	int cols = static_cast<int>(width);

	// Mat class
	// get the data of the image 
	// store the data of the image in vec1

	Mat matimg(img);

	vector<vector<double>> vec1(rows, vector<double>(cols));

	int k = 1;

	for (int i = 0; i < rows; i++) 
	{
		for (int j = 0; j < cols; j++)
		{
			unsigned char temp;
			temp = ((uchar*) matimg.data + i * matimg.step)[j * matimg.elemSize() + k];
			vec1[i][j] = static_cast<double>(temp);
			//cout << i << ", " << j << " -> " << static_cast<double>(temp) << endl;
		}
	}

	// Finding 2D DWT Transform of the image
	// using symetric extension algorithm

	string nm = "db4"; // the type of DWT transform
	int J = 3; // the level of refinement

	vector<int> length;
	vector<double> output;
	vector<double> flag;

	// apply the DWT to image data

	cout << " --> apply dwt to image data" << endl;

	dwt_2d_sym(vec1, J, nm, output, flag, length);

	cout << " --> dwt data elements" << endl;

	// i can not figure out what is the content of "output" vector

	int kk1;
	int kk2;
	int kk3;
	int kA = length[0] * length[1];
        int kB = length[2] * length[3];
	int kC = length[4] * length[5];

	// matrix --> 1 --> 246 x 141

	cout << "new --> 1 --> 246 x 141" << endl;
	
	kk1 = 0;	

	for (int i = kk1 * kA ; i != (kk1+1) * kA; i++)
	{
		cout << i+1 << " --> " << output[i] << endl;
	}

	// matrix --> 2 -->  246 x 141

	cout << "new --> 2 --> 246 x 141" << endl;

	kk1 = 1;

    	for (int i = kk1 * kA; i != (kk1+1) * kA; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// matrix --> 3 --> 246 x 141

        cout << "new --> 3 --> 246 x 141" << endl;

	kk1 = 2;

        for (int i = kk1 * kA; i != (kk1+1)*kA; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }
	
	// matrix --> 4 --> 246 x 141

        cout << "new --> 4 --> 246 x 141" << endl;

	kk1 = 3;

        for (int i = kk1 * kA; i != (kk1+1) * kA; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// matrix --> 5 --> 485 x 275

        cout << "new --> 5 --> 485 x 275" << endl;

        kk1 = 4;
	kk2 = 1;

        for (int i = kk1*kA+(kk2-1)*kB; i != kk1*kA+kk2*kB; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// matrix --> 6 --> 485 x 275

        cout << "new --> 6 --> 485 x 275" << endl;

        kk1 = 4;
        kk2 = 2;

        for (int i = kk1*kA+(kk2-1)*kB; i != kk1*kA+kk2*kB; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// matrix --> 7 --> 485 x 275

        cout << "new --> 7 --> 485 x 275" << endl;

        kk1 = 4;
        kk2 = 3;
        
        for (int i = kk1*kA+(kk2-1)*kB; i != kk1*kA+kk2*kB; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// matrix --> 8 --> 963 x 543

        cout << "new --> 8 --> 963 x 543" << endl;

        kk1 = 4;
        kk2 = 3;
	kk3 = 1;

        for (int i = kk1*kA+kk2*kB+(kk3-1)*kC; i != kk1*kA+kk2*kB+kk3*kC; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// matrix --> 9 --> 963 x 543

        cout << "new --> 9 --> 963 x 543" << endl;

        kk1 = 4;
        kk2 = 3;
        kk3 = 2;

        for (int i = kk1*kA+kk2*kB+(kk3-1)*kC; i != kk1*kA+kk2*kB+kk3*kC; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// matrix --> 10 --> 963 x 543

        cout << "new --> 10 --> 963 x 543" << endl;

        kk1 = 4;
        kk2 = 3;
        kk3 = 3;

        for (int i = kk1*kA+kk2*kB+(kk3-1)*kC; i != kk1*kA+kk2*kB+kk3*kC; i++)
        {
                cout << i+1 << " --> " << output[i] << endl;
        }

	// the size of "flag" vector is 1
	// and the content is flag[0] = 3
	// I do not know the meaning of it

	cout << " --> flag data" << endl;

	for (unsigned int i = 0; i != flag.size(); i++)
	{
		cout << i << " --> " << flag[i] << endl;
	}

	// here are the dimensions for the submartices
	// exact the same as in Mathematica

	cout << " --> length data" << endl;

	for (unsigned int i = 0; i != length.size(); i++)
	{
		cout << i << " --> " << length[i] << endl;
	}

	// This algorithm computes DWT of image of any given size.
	// Together with convolution and subsampling operations
	// it is clear that subsampled images are of different length than
	// dyadic length images. In order to compute the "effective" size of DWT
	// we do additional calculations

	double max;
	vector<int> length2;

	dwt_output_dim_sym(length, length2, J);

	// here are the contents of the length2 vector

        cout << " --> length2 data" << endl;

        for (unsigned int i = 0; i != length2.size(); i++)
        {
                cout << i << " --> " << length2[i] << endl;
        }

	// length2 is gives the integer vector that contains
	// the size of subimages that will
	// combine to form the displayed output image.
	// The last two entries of length2 gives the
	// size of DWT (rows_n by cols_n)

	int siz = length2.size();
	int rows_n = length2[siz-2];
	int cols_n = length2[siz-1];

	vector<vector<double>> dwtdisp(rows_n, vector<double>(cols_n));

	dispDWT(output, dwtdisp, length, length2, J);

	// dispDWT returns the 2D object dwtdisp 
	// which will be displayed using OPENCV's image
	// handling functions

	vector<vector<double>> dwt_output = dwtdisp;

	maxval(dwt_output, max);

	// max value is needed to take care of overflow which happens because
	// of convolution operations performed on unsigned 8 bit images

	// displaying scaled image
	// creating image in OpenCV

	IplImage * cvImg; // image used for output
	CvSize imgSize; // size of output image

	imgSize.width = cols_n;
	imgSize.height = rows_n;

	cvImg = cvCreateImage(imgSize, 8, 1);

	// dwt_hold is created to hold the dwt output
	// as further operations need to be
	// carried out on dwt_output in order to display scaled images

	vector<vector<double>> dwt_hold(rows_n, vector<double>(cols_n));

	dwt_hold = dwt_output;

	// Setting coefficients of created image to the scaled DWT output values

	for (int i = 0; i < imgSize.height; i++)
	{
		for (int j = 0; j < imgSize.width; j++)
		{
			if (dwt_output[i][j] <= 0.0)
			{
				dwt_output[i][j] = 0.0;
			}

			if (i <= (length2[0]) && j <= (length2[1]))
			{
				((uchar*)(cvImg->imageData + cvImg->widthStep*i))[j] =
					static_cast<char>((dwt_output[i][j] / max) * 255.0);
			}
			else
			{
				((uchar*)(cvImg->imageData + cvImg->widthStep*i))[j] =
					static_cast<char>(dwt_output[i][j]);
			}
		}
	}

	// display cvImg

	cvNamedWindow("DWT Image", 1); // creation of a visualisation window

	cvShowImage("DWT Image", cvImg); // image visualisation

	cvWaitKey();

	cvDestroyWindow("DWT Image");

	cvSaveImage("dwt.bmp",cvImg);

	// finding IDWT

	vector<vector<double>> idwt_output(rows, vector<double>(cols));

	idwt_2d_sym(output, flag, nm, idwt_output, length);

	// displaying reconstructed image

	IplImage * dvImg;
	CvSize dvSize;

	dvSize.width = idwt_output[0].size();
	dvSize.height = idwt_output.size();

	cout << " --> 5" << endl;

	cout << idwt_output.size() << " , " << idwt_output[0].size() << endl;

	cout << " --> 6" << endl;

	dvImg = cvCreateImage(dvSize, 8, 1);

	for (int i = 0; i < dvSize.height; i++)
	{
		for (int j = 0; j < dvSize.width; j++)
		{
			((uchar*)(dvImg->imageData + dvImg->widthStep*i))[j] =
				static_cast<char>(idwt_output[i][j]);
		}
	}

	// display reconstructed image

	cvNamedWindow("Reconstructed Image", 1); // creation of a visualisation window
	cvShowImage("Reconstructed Image", dvImg); // image visualisation
	cvWaitKey();
	cvDestroyWindow("Reconstructed Image");
	cvSaveImage("recon.bmp", dvImg);

	return 0;
}

//======//
// FINI //
//======//

